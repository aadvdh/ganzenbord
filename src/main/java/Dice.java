import java.util.Random;

public class Dice {

    Random random;

    public Dice() {
        this.random = new Random();
    }

    public int rollDice() {
        int roll = this.random.nextInt(6) + 1;
        return roll;
    }
}