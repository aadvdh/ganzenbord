import java.util.*;

public class Ganzenbord {

    private Dice dice;
    private List<Player> players;
    private Optional<Player> playerInWell;
    private Queue<Player> playerQueue;
    Player currentPlayer;
    private static Scanner in;




    public Ganzenbord() {
        dice = new Dice();
        players = new ArrayList<>();
        playerInWell = Optional.empty();
        in = new Scanner(System.in);


    }

    public static void main(String[] args) {
        Ganzenbord game = new Ganzenbord();

        //Minimaal 2 spelers
        while(game.playerCount() < 2) {
            game.initializePlayers();
        }

        //Zet spelers in willekeurige volgorde en zet ze in een rij
        Collections.shuffle(game.players);
        game.playerQueue = new LinkedList<>(game.players);

        while(true) {
            System.out.println("Typ g om de dobbelsteen te gooien");

            game.currentPlayer = game.getPlayerFromQueue();

            String input = in.nextLine();
            if(!input.equals("g")) {
                continue;
            }

            if(game.currentPlayer.shouldSkipTurn()) {
                System.out.println("Helaas je moet een beurt wachten");
                game.currentPlayer.decrementSkippedTurn();
                System.out.println("Je moet hierna nog " + game.currentPlayer.getSkippedTurns() + "beurten wachten");
                continue;
            }

            if(game.isPlayerInWell(game.currentPlayer)) {
                System.out.println(game.currentPlayer.getName() + " zit nog steeds vast in de put");
                continue;
            }

            game.movePlayer();

            if(game.currentPlayer.getPosition() == 63) {
                System.out.println(game.currentPlayer.getName() + " heeft gewonnen");
                break;
            }
            if(game.currentPlayer.getPosition() > 63) {
                System.out.println("Te ver gegooid");
                int rollBack = game.currentPlayer.getPosition() % 63;
                int newPosition = 63 - rollBack;
                System.out.println("Je moet " + rollBack + " stappen terug vanaf 63");
                game.currentPlayer.setPosition(newPosition);
            }
            else if(game.currentPlayer.getPosition() % 10 == 0) {
                System.out.println("Dubbel gooien");
                int extraRoll = game.currentPlayer.getPosition() + game.currentPlayer.getLastRoll();
                game.currentPlayer.setPosition(extraRoll);
            }
            else if(game.currentPlayer.getPosition() == 23) {
                System.out.println(game.currentPlayer.getName() + " staat op 23");
                System.out.println("Game over");
                break;
            }
            else if(game.currentPlayer.getPosition() == 25 || game.currentPlayer.getPosition() == 45 || game.currentPlayer.getPosition() == 58) {
                System.out.println("Terug naar start");
               game.currentPlayer.setPosition(0);
            }
            else if(game.currentPlayer.getPosition() == 6) {
                System.out.println("Brug");
                game.currentPlayer.setPosition(12);

            }
            else if(game.currentPlayer.getPosition() == 42) {
                System.out.println("Doolhof");
                game.currentPlayer.setPosition(39);
            }
            else if(game.currentPlayer.getPosition() == 19) {
                System.out.println("Sla de volgende beurt over.");
                game.currentPlayer.addSkippedTurns(1);
            }
            else if(game.currentPlayer.getPosition() == 52) {
                System.out.println("Gevangenis, sla drie beurten over");
                game.currentPlayer.addSkippedTurns(3);
            }
            else if(game.currentPlayer.getPosition() == 31) {

                System.out.println("Je zit in de put helaas, wacht tot een andere speler in de put valt.");
                game.playerInWell = Optional.of(game.currentPlayer);

            }

            System.out.println(game.currentPlayer.getName() + " staat op positie " + game.currentPlayer.getPosition());


        }
    }

    private void addPlayer(String name) {
        Player newPlayer = new Player(name);
        this.players.add(newPlayer);
    }

    //haal de voorste speler uit de rij en voeg hem toe aan het einde
    private Player getPlayerFromQueue() {
        Player player = this.playerQueue.poll();
        this.playerQueue.add(player);
        return player;
    }

    private int playerCount() {
       return this.players.size();
    }

    //Check of speler in put zit.
    private boolean isPlayerInWell(Player player) {
        if(this.playerInWell.isPresent()) {
           return player.equals(this.playerInWell.get());
        }
        return false;
    }

    //Gooi dobbelsteen en verzet positie van speler
    private void movePlayer() {
        int roll = this.dice.rollDice();
        this.currentPlayer.setLastRoll(roll);
        this.currentPlayer.setPosition(this.currentPlayer.getPosition() + roll);
    }

    private void initializePlayers() {
        while(true) {
            System.out.println("typ start om het spel te beginnen");
            System.out.println("Voeg een speler toe: ");
            System.out.print("Naam: ");
            String name = in.nextLine();
            if(name.equals("start")) {
                break;
            }
            this.addPlayer(name);
        }
    }





}