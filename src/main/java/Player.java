import java.util.function.DoubleToIntFunction;

public class Player {

    private String name;
    private int position;
    private int skippedTurns;
    private int lastRoll;


    public Player(String name) {
        this.name = name;
        this.position = 0;
        this.skippedTurns = 0;
        this.lastRoll = 0;


    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSkippedTurns() {
        return skippedTurns;
    }

    public boolean shouldSkipTurn() {
        if(skippedTurns > 0) {
            return true;
        }
        return false;
    }

    public void setSkippedTurns(int skippedTurns) {
        this.skippedTurns = skippedTurns;
    }

    public void addSkippedTurns(int turns) {
        this.skippedTurns = skippedTurns + turns;
    }

    public void decrementSkippedTurn() {
        if(skippedTurns == 0) {
            return;
        }
        this.skippedTurns = skippedTurns - 1;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
    public void setLastRoll(int roll) {
        this.lastRoll = roll;
    }
    public int getLastRoll() {
        return this.lastRoll;
    }

}